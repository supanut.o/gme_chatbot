package main

import (
	"encoding/json"
	"fmt"
	"gme-chatbot-api/model"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	//models "gme/chatbot-api/config"
	"github.com/line/line-bot-sdk-go/linebot"
)

// CheckAuthorize .
func CheckAuthorize(userID string, groupID string, roomID string, eventMsg string, logEventMsg string) (status int, detail string) {
	v, err := GetConfig("config", "./")
	urlAPI := v.GetString("api.url")
	var AuthenStatus = -1
	var AuthenDetail = ""

	log.Println("CheckAuthorize() Step1 => urlAPI:", urlAPI)

	newReq := new(UsersDetailReq)
	newReq.UserID = userID
	newReq.GroupID = groupID
	newReq.RoomID = roomID
	newReq.EventMsg = eventMsg
	newReq.LogEventMsg = logEventMsg

	j, err := json.Marshal(newReq)
	if err != nil {
		log.Println(err.Error())
	}

	log.Println("CheckAuthorize() Step2 => json:", string(j))

	payload := strings.NewReader(string(j))
	req, err := http.NewRequest("POST", urlAPI, payload)

	if err != nil {
		log.Println(err.Error())
	}

	req.Header.Add("content-type", "application/json")

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {

		data, _ := ioutil.ReadAll(response.Body)
		log.Println("CheckAuthorize() Step3 => json: ", string(data))
		newRes := new(UsersDetailRes)

		err = json.Unmarshal(data, &newRes)
		if err != nil {
			log.Println(err.Error())
		}
		log.Println("CheckAuthorize() Step4 => newRes.UserID: ", newRes.UserID, " => newRes.AuthenStatus: ", newRes.AuthenStatus)
		if newRes.UserID != "" && newRes.AuthenStatus >= 0 {
			AuthenStatus = newRes.AuthenStatus

		}
		AuthenDetail = newRes.AuthenStatusDesc
	}

	log.Println("CheckAuthorize() Step5 => AuthenStatus:", AuthenStatus, " => AuthenDetail: ", AuthenDetail)

	return AuthenStatus, AuthenDetail
}

func replyMessage(msgData string, msgType string, msgReturn string, replyToken string) {

	if msgType == "FlexWithJSON" {

		bot, err := linebot.New(
			os.Getenv("CHANNEL_SECRET"),
			os.Getenv("CHANNEL_TOKEN"),
		)

		log.Println("replyMessage-FlexWithJSON()->UnmarshalFlexMessageJSON")
		// Unmarshal JSON
		flexContainer, err := linebot.UnmarshalFlexMessageJSON([]byte(msgData))
		if err != nil {
			log.Print(err)
		}
		log.Println("replyMessage-FlexWithJSON()->NewFlexMessage")
		// New Flex Message
		flexMessage := linebot.NewFlexMessage(msgReturn, flexContainer)

		log.Println("replyMessage-FlexWithJSON()->ReplyMessage")
		if _, err = bot.ReplyMessage(replyToken, flexMessage).Do(); err != nil {
			log.Print(err)
		}

	} else if msgType == "Sticker" {

		bot, err := linebot.New(
			os.Getenv("CHANNEL_SECRET"),
			os.Getenv("CHANNEL_TOKEN"),
		)
		log.Println("replyMessage-FlexWithJSON()->ReplyMessage")

		jsonMap := make(map[string]interface{})
		errmsgData := json.Unmarshal([]byte(msgData), &jsonMap)
		_ = errmsgData
		var _type, _packageId, _stickerId string
		for key, value := range jsonMap {
			log.Println("index : ", key, " - value : ", value)
			if key == "type" {
				_type = value.(string)
			} else if key == "packageId" {
				_packageId = value.(string)
			} else if key == "stickerId" {
				_stickerId = value.(string)
			}
		}
		_ = _type
		_ = _packageId
		_ = _stickerId

		message := linebot.NewStickerMessage(_packageId, _stickerId)
		//result, err := bot.ReplyMessage(replyToken, message)
		if _, err = bot.ReplyMessage(replyToken, message).Do(); err != nil {
			log.Print(err)
		}

	} else {

		bot, err := linebot.New(
			os.Getenv("CHANNEL_SECRET"),
			os.Getenv("CHANNEL_TOKEN"),
		)
		log.Println("replyMessage-FlexWithJSON()->ReplyMessage")
		if _, err = bot.ReplyMessage(replyToken, linebot.NewTextMessage(msgData)).Do(); err != nil {
			log.Print(err)
		}
	}
}

// msg Data
func msgRegister(userID string, authenDetail string, replyToken string) {
	log.Println("msgRegister() Step1 => userID:", userID, " => authenDetail: ", authenDetail, " => replyToken: ", replyToken)

	v, _ := GetConfig("config", "./")
	urlRegister := v.GetString("api.register")
	log.Println("msgRegister() Step2 => urlRegister:", urlRegister)

	var flexData = `
	{
		"type": "bubble",
		"hero": {
		  "type": "image",
		  "url": "https://deploy-react-gme-chatbot.firebaseapp.com/assets/img/KTB_1.jpg",
		  "size": "xs",
		  "aspectRatio": "1:1",
		  "aspectMode": "cover",
		  "action": {
			"type": "uri",
			"uri": "http://linecorp.com/"
		  }
		},
		"body": {
		  "type": "box",
		  "layout": "vertical",
		  "contents": [
			{
			  "type": "text",
			  "text": "🎄🎄🎄 ลงทะเบียนใช้งาน 🎄🎄🎄",
			  "weight": "bold",
			  "size": "md"
			},
			{
			  "type": "box",
			  "layout": "baseline",
			  "margin": "none",
			  "contents": [
				{
				  "type": "text",
				  "text": "%[1]s",
				  "size": "xs",
				  "color": "#999999",
				  "margin": "none",
				  "flex": 0
				}
			  ]
			}
		  ]
		},
		"footer": {
		  "type": "box",
		  "layout": "vertical",
		  "spacing": "sm",
		  "backgroundColor": "#0000000F",
		  "contents": [
			{
			  "type": "button",
			  "style": "link",
			  "height": "sm",
			  "action": {
				"type": "uri",
				"label": "Register",
				"uri": "%[2]s"
		  }
			},
			{
			  "type": "spacer",
			  "size": "sm"
			}
		  ],
		  "flex": 0
		}
	  }`

	txtData := fmt.Sprintf(flexData, authenDetail, urlRegister+"?uid="+userID)
	//log.Println("msgRegister() Step3 => txtData:", txtData)
	replyMessage(txtData, "FlexWithJSON", "Register", replyToken)
}

func msgRegisterRedirectToPrivateChatBot(userID string, authenDetail string, replyToken string) {
	log.Println("msgRegisterRedirectToPrivateChatBot() Step1 => userID:", userID, " => authenDetail: ", authenDetail, " => replyToken: ", replyToken)

	var flexData = `
	{
		"type": "bubble",
		"hero": {
		  "type": "image",
		  "url": "https://deploy-react-gme-chatbot.firebaseapp.com/assets/img/KTB_1.jpg",
		  "size": "xs",
		  "aspectRatio": "1:1",
		  "aspectMode": "cover",
		  "action": {
			"type": "uri",
			"uri": "http://linecorp.com/"
		  }
		},
		"body": {
		  "type": "box",
		  "layout": "vertical",
		  "contents": [
			{
			  "type": "text",
			  "text": "🎄🎄🎄 ลงทะเบียนใช้งาน 🎄🎄🎄",
			  "weight": "bold",
			  "size": "md"
			},
			{
			  "type": "box",
			  "layout": "baseline",
			  "margin": "none",
			  "contents": [
				{
				  "type": "text",
				  "text": "%[1]s",
				  "size": "xs",
				  "color": "#999999",
				  "margin": "none",
				  "flex": 0
				}
			  ]
			},
			{
			  "type": "box",
			  "layout": "baseline",
			  "margin": "xs",
			  "contents": [
				{
				  "type": "text",
				  "text": "%[2]s",
				  "size": "xs",
				  "color": "#999999",
				  "margin": "none",
				  "flex": 0
				}
			  ]
			}
		  ]
		},
		"footer": {
		  "type": "box",
		  "layout": "vertical",
		  "spacing": "sm",
		  "backgroundColor": "#0000000F",
		  "contents": [
			{
			  "type": "button",
			  "style": "link",
			  "height": "sm",
			  "action": {
				"type": "uri",
				"label": "Private ChatBot",
				"uri": "%[3]s"
		  }
			},
			{
			  "type": "spacer",
			  "size": "sm"
			}
		  ],
		  "flex": 0
		}
	  }`

	txtData := fmt.Sprintf(flexData, authenDetail, "กรุณากด Link เพื่อลงทะเบียนที่ Private ChatBot", "line://oaMessage/@346ufxiy/Register")
	//log.Println("msgRegisterRedirectToPrivateChatBot() Step2 => txtData:", txtData)
	replyMessage(txtData, "FlexWithJSON", "RedirectRegister", replyToken)
}

func msgUnactive(userID string, authenDetail string, replyToken string) {
	v, _ := GetConfig("config", "./")
	urlUnlock := v.GetString("api.unlock")
	//fmt.Println(urlUnlock)
	var flexData = `
	{
		"type": "bubble",
		"hero": {
		  "type": "image",
		  "url": "https://deploy-react-gme-chatbot.firebaseapp.com/assets/img/SUPPORT_PROFILE.jpg",
		  "size": "xs",
		  "aspectRatio": "1:1",
		  "aspectMode": "cover"
		},
		"body": {
		  "type": "box",
		  "layout": "vertical",
		  "contents": [
			{
			  "type": "text",
			  "text": "🎅🎅 ทำรายการขอปลดล็อค 🎅🎅",
			  "weight": "bold",
			  "size": "md"
			},
			{
			  "type": "box",
			  "layout": "baseline",
			  "margin": "none",
			  "contents": [
				{
				  "type": "text",
				  "text": "%[1]s",
				  "size": "xs",
				   "wrap": true,
				  "color": "#999999",
				  "margin": "none"
				}
			  ]
			} 
		  ]
		},
		"footer": {
		  "type": "box",
		  "layout": "vertical",
		  "spacing": "sm",
		  "backgroundColor": "#0000000F",
		  "contents": [
			{
			  "type": "button",
			  "style": "link",
			  "height": "sm",
			  "action": {
				"type": "uri",
				"label": "Website",
				"uri": "%[2]s"
			   }
			},
			{
			  "type": "spacer",
			  "size": "sm"
			}
		  ],
		  "flex": 0
		}
	  }`

	txtData := fmt.Sprintf(flexData, authenDetail, urlUnlock+"?uid="+userID)
	replyMessage(txtData, "FlexWithJSON", "User Is Locked", replyToken)
}

func msgUnactiveRedirectToPrivateChatBot(userID string, authenDetail string, replyToken string) {
	log.Println("msgUnactiveRedirectToPrivateChatBot() Step1 => userID:", userID, " => authenDetail: ", authenDetail, " => replyToken: ", replyToken)

	var flexData = `
	{
		"type": "bubble",
		"hero": {
		  "type": "image",
		  "url": "https://deploy-react-gme-chatbot.firebaseapp.com/assets/img/SUPPORT_PROFILE.jpg",
		  "size": "xs",
		  "aspectRatio": "1:1",
		  "aspectMode": "cover"
		},
		"body": {
		  "type": "box",
		  "layout": "vertical",
		  "contents": [
			{
			  "type": "text",
			  "text": "🎅🎅 ทำรายการขอปลดล็อค 🎅🎅",
			  "weight": "bold",
			  "size": "md"
			},
			{
			  "type": "box",
			  "layout": "baseline",
			  "margin": "none",
			  "contents": [
				{
				  "type": "text",
				  "text": "%[1]s",
				  "size": "xs",
				   "wrap": true,
				  "color": "#999999",
				  "margin": "none"
				}
			  ]
			},
			{
			  "type": "box",
			  "layout": "baseline",
			  "margin": "xs",
			  "contents": [
				{
				  "type": "text",
				  "text": "%[2]s",
				  "size": "xs",
				  "color": "#999999",
				  "margin": "none",
				  "flex": 0
				}
			  ]
			} 
		  ]
		},
		"footer": {
			"type": "box",
			"layout": "vertical",
			"spacing": "sm",
			"backgroundColor": "#0000000F",
			"contents": [
			  {
				"type": "button",
				"style": "link",
				"height": "sm",
				"action": {
				  "type": "uri",
				  "label": "Private ChatBot",
				  "uri": "%[3]s"
			}
			  },
			  {
				"type": "spacer",
				"size": "sm"
			  }
			],
			"flex": 0
		}
	  }`

	txtData := fmt.Sprintf(flexData, authenDetail, "กรุณากด Link เพื่อขอปลดล็อคที่ Private ChatBot", "line://oaMessage/@346ufxiy/UnLockUser")
	//log.Println("msgUnactiveRedirectToPrivateChatBot() Step2 => txtData:", txtData)
	replyMessage(txtData, "FlexWithJSON", "RedirectUserUnLock", replyToken)
}

func menuList() string {
	var data = `{
		{
			"type": "template",
			"altText": "this is a confirm template",
			"template": {
			  "type": "confirm",
			  "actions": [
				{
				  "type": "message",
				  "label": "Yes",
				  "text": "Yes"
				},
				{
				  "type": "message",
				  "label": "No",
				  "text": "No"
				}
			  ],
			  "text": "Continue?"
			}
		  }
	  }`
	return data
}

func generateMsgBatchLogStatus(urlDetail string, eventUserID string, msgText string) (string, bool) {
	//===> 1.Default JSON Text
	//===> 2.Get API for Recieve Data (dbBatchName, dbBatchStatus, dbBatchTextDetail)
	//===> 3.Loop Data to create JSON Text with Pattern Line FLEX Message
	//===> 4.Return FLEX JSON Message

	v, err := GetConfig("config", "./")
	urlAPI := v.GetString("api.urlMsgBatchLogFlex")
	//urlAPI := "https://de918f49.ngrok.io/getMessageBatchLogFlexPost"

	log.Println("generateMsgBatchLogStatus() Step1 => urlAPI:", urlAPI)

	newReq := new(MessageLine)
	newReq.UserID = eventUserID
	newReq.EventMsg = msgText

	j, err := json.Marshal(newReq)
	if err != nil {
		log.Println(err.Error())
	}

	log.Println("generateMsgBatchLogStatus() Step2 => json:", string(j))

	payload := strings.NewReader(string(j))
	req, err := http.NewRequest("POST", urlAPI, payload)

	if err != nil {
		log.Println(err.Error())
	}

	req.Header.Add("content-type", "application/json")

	client := &http.Client{}
	response, err := client.Do(req)
	log.Println("generateMsgBatchLogStatus() Step3")

	DataForBatchLogFlexMsgs := []DataForBatchLogFlexMsg{}
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {

		data, _ := ioutil.ReadAll(response.Body)
		log.Println("generateMsgBatchLogStatus() Step4 => json:", string(data))

		err = json.Unmarshal(data, &DataForBatchLogFlexMsgs)
		if err != nil {
			log.Println(err.Error())
		}
		//log.Println("generateMsgBatchLogStatus() Step7 => newRes.BatchName:", DataForBatchLogFlexMsgs[0].BatchName)
		//log.Println("generateMsgBatchLogStatus() Step8 => newRes.BatchStatus:", DataForBatchLogFlexMsgs[0].BatchStatus)
		//log.Println("generateMsgBatchLogStatus() Step9 => newRes.BatchTextDetail:", DataForBatchLogFlexMsgs[0].BatchTextDetail)
	}

	log.Println("generateMsgBatchLogStatus() Step5 => Array Length:", len(DataForBatchLogFlexMsgs))
	_countArrData := len(DataForBatchLogFlexMsgs)
	_ = _countArrData

	var batchName = "GL_01" //===> หาจาก DB
	var urlResult = urlDetail + "?BatchName=" + batchName + "&uid=" + eventUserID

	log.Println("generateMsgBatchLogStatus() Step6")

	var _jsonBox = `{
		"type": "bubble",
		"styles": {
		  "footer": {
			"separator": true
		  }
		},
		%s
	}`

	var _jsonBody = `  "body": {
		"type": "box",
		"layout": "vertical",
		"contents": [
		  {
			"type": "text",
			"text": "GL",
			"weight": "bold",
			"color": "#3f51b5",
			"margin": "md"
		  },
		  %s
		]
	  }`

	var _jsonDetail = ``
	for index, element := range DataForBatchLogFlexMsgs {
		//===> LOOP DATA FROM API CORE
		var _BatchName = DataForBatchLogFlexMsgs[index].BatchName
		var _BatchStatus = DataForBatchLogFlexMsgs[index].BatchStatus
		var _BatchTextDetail = DataForBatchLogFlexMsgs[index].BatchTextDetail
		urlResult = urlDetail + "?BatchName=" + _BatchName + "&uid=" + eventUserID

		var _colorLog = "#1c1fd9"
		var _colorStatus = "#1c1fd9"
		if _BatchStatus == "START" {
			_colorStatus = "#d3e016" //STA
		} else if _BatchStatus == "FINISH" {
			_colorStatus = "#8bc34a" //FIN
		} else if _BatchStatus == "ERROR" {
			_colorStatus = "#db2121" //ERR
		} else {
			_colorStatus = "#1c1fd9"
		}

		log.Println("generateMsgBatchLogStatus() Step7 => Index :", index, " Element :", element, " BatchName :", _BatchName, " BatchStatus :", _BatchStatus, " BatchTextDetail :", _BatchTextDetail)

		_jsonDetail += `{
			"type": "box",
			"layout": "baseline",
			"spacing": "sm",
			"paddingTop": "5px",
			"contents": [
			  {
				"type": "text",
				"text": "` + _BatchName + `",
				"wrap": true,
				"size": "md",
				"flex": 5
			  }
			]
		  },
		  {
			"type": "box",
			"layout": "baseline",
			"spacing": "sm",
			"contents": [
			  {
				"type": "text",
				"text": "Status",
				"color": "#aaaaaa",
				"size": "sm",
				"flex": 1
			  },
			  {
				"type": "text",
				"text": "` + _BatchStatus + `",
				"color": "` + _colorStatus + `",
				"size": "lg",
				"weight": "bold",
				"margin": "md",
				"flex": 3,
				"action": {
				  "type": "message",
				  "label": "` + _BatchTextDetail + `",
				  "text": "` + _BatchTextDetail + `"
				}
			  },
			  {
				"type": "text",
				"text": "Log",
				"color": "` + _colorLog + `",
				"size": "lg",
				"flex": 2,
				"action": {
				  "type": "uri",
				  "label": "LOG",
				  "uri": "` + urlResult + `"
				}
			  }
			]
		  },
		  {
			"type": "separator"
		  `
		if index == _countArrData-1 {
			_jsonDetail += `}`
		} else {
			_jsonDetail += `},`
		}
	}

	var jsonFlexMsg = ""
	_ = jsonFlexMsg
	_json1 := fmt.Sprintf(_jsonBox, _jsonBody)
	//log.Println("JSON STEP 1")
	_jsonDetailSum := _jsonDetail
	//log.Println("JSON STEP 2")
	jsonFlexMsg = fmt.Sprintf(_json1, _jsonDetailSum)
	//log.Println("JSON STEP 3")
	//log.Println(jsonFlexMsg)
	log.Println("generateMsgBatchLogStatus() Step8 => Return")
	return jsonFlexMsg, true
}

func generateMsgBatchLogStatusTest(urlDetail string, eventUserID string, msgText string) (string, bool) {
	//===> 1.Default JSON Text
	//===> 2.Get API for Recieve Data
	//===> 3.Loop Data to create JSON Text with Pattern Line FLEX Message
	//===> 4.Return FLEX JSON Message

	var batchName = "GL_01"
	var urlResult = urlDetail + "?BatchName=" + batchName + "&uid=" + eventUserID

	log.Println("generateMsgBatchLogStatusTest()")

	var _jsonBox = `{"type": "bubble","styles": {"footer": {"separator": true}}, %s }`

	var _jsonBody = `"body": {"type": "box","layout": "vertical","contents": [{"type": "text","text": "GL","weight": "bold","color": "#3f51b5","margin": "md"}, %s ]}`

	var _jsonDetail = `{
        "type": "box",
        "layout": "baseline",
        "spacing": "sm",
        "paddingTop": "5px",
        "contents": [
          {
            "type": "text",
            "text": "batch_gl_01 test file test file.xls",
            "wrap": true,
            "size": "md",
            "flex": 5
          }
        ]
	  },
	  {
        "type": "box",
        "layout": "baseline",
        "spacing": "sm",
        "contents": [
          {
            "type": "text",
            "text": "Status",
            "color": "#aaaaaa",
            "size": "sm",
            "flex": 1
          },
          {
            "type": "text",
            "text": "Running",
            "color": "#8bc34a",
            "size": "lg",
            "weight": "bold",
            "margin": "md",
            "flex": 3,
            "action": {
              "type": "message",
              "label": "FileBatchGL01",
              "text": "FileBatchGL01"
            }
          },
          {
            "type": "text",
            "text": "Log",
            "color": "#ff5722",
            "size": "lg",
            "flex": 2,
            "action": {
              "type": "uri",
              "label": "LOG",
              "uri": "` + urlResult + `"
            }
          }
        ]
      },
      {
        "type": "separator"
	  },`

	var _jsonDetail2 = `{
        "type": "box",
        "layout": "baseline",
        "spacing": "sm",
        "paddingTop": "5px",
        "contents": [
          {
            "type": "text",
            "text": "batch_gl_01 test file test file.xls",
            "wrap": true,
            "size": "md",
            "flex": 5
          }
        ]
	  },
	  {
        "type": "box",
        "layout": "baseline",
        "spacing": "sm",
        "contents": [
          {
            "type": "text",
            "text": "Status",
            "color": "#aaaaaa",
            "size": "sm",
            "flex": 1
          },
          {
            "type": "text",
            "text": "Running",
            "color": "#8bc34a",
            "size": "lg",
            "weight": "bold",
            "margin": "md",
            "flex": 3,
            "action": {
              "type": "message",
              "label": "FileBatchGL01",
              "text": "FileBatchGL01"
            }
          },
          {
            "type": "text",
            "text": "Log",
            "color": "#ff5722",
            "size": "lg",
            "flex": 2,
            "action": {
              "type": "uri",
              "label": "LOG",
              "uri": "` + urlResult + `"
            }
          }
        ]
      },
      {
        "type": "separator"
      }`

	var jsonFlexMsg = ``
	_ = jsonFlexMsg
	_json1 := fmt.Sprintf(_jsonBox, _jsonBody)
	log.Println("_json1: ", _json1)
	_jsonDetailSum := _jsonDetail + _jsonDetail2
	log.Println("_jsonDetailSum: ", _jsonDetailSum)
	jsonFlexMsg = fmt.Sprintf(_json1, _jsonDetailSum)
	log.Println("jsonFlexMsg: ", jsonFlexMsg)

	var anything interface{}
	json.Unmarshal([]byte(jsonFlexMsg), &anything)
	switch v := anything.(type) {
	case float64:
		// v is an float64
		fmt.Printf("NUMBER: %f\n", v)
	case string:
		// v is a string
		fmt.Printf("STRING: %s\n", v)
	default:
		fmt.Printf("I don't know how to handle this!")
	}
	log.Println("return jsonFlexMsg: ", jsonFlexMsg)
	return jsonFlexMsg, true
}

func generateMsgBatchGLDetailByBatchName(eventUserID string, msgText string) (string, bool) {
	//===> 1.Default JSON Text
	//===> 2.Get API for Recieve Data (dbBatchName, dbBatchStatus, dbBatchTextDetail)
	//===> 3.Loop Data to create JSON Text with Pattern Line FLEX Message
	//===> 4.Return FLEX JSON Message

	v, err := GetConfig("config", "./")
	urlAPI := v.GetString("api.urlMsgBatchGLDetail")

	log.Println("generateMsgBatchGLDetailByBatchName() Step1 => urlAPI:", urlAPI)

	newReq := new(MessageLine)
	newReq.UserID = eventUserID
	newReq.EventMsg = msgText

	j, err := json.Marshal(newReq)
	if err != nil {
		log.Println(err.Error())
	}

	log.Println("generateMsgBatchGLDetailByBatchName() Step2 => json:", string(j))

	payload := strings.NewReader(string(j))
	req, err := http.NewRequest("POST", urlAPI, payload)

	if err != nil {
		log.Println(err.Error())
	}

	req.Header.Add("content-type", "application/json")

	client := &http.Client{}
	response, err := client.Do(req)
	log.Println("generateMsgBatchGLDetailByBatchName() Step3")

	DataForBatchGLDetailFlexMsgs := []DataForBatchGLDetailFlexMsg{}
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {

		data, _ := ioutil.ReadAll(response.Body)
		log.Println("generateMsgBatchGLDetailByBatchName() Step4 => json:", string(data))

		err = json.Unmarshal(data, &DataForBatchGLDetailFlexMsgs)
		if err != nil {
			log.Println(err.Error())
		}
	}

	log.Println("generateMsgBatchGLDetailByBatchName() Step5 => Array Length:", len(DataForBatchGLDetailFlexMsgs))
	_countArrData := len(DataForBatchGLDetailFlexMsgs)
	_ = _countArrData

	log.Println("generateMsgBatchGLDetailByBatchName() Step6")

	var _jsonBox = `{
		"type":"bubble",
		"body":{
		   "type":"box",
		   "layout":"vertical",
		   "paddingAll":"10px",
		   "contents":[
			  {
				 "type":"text",
				 "text":"Detail Batch",
				 "weight":"bold",
				 "size":"xl"
			  },
			  {
				 "type":"box",
				 "layout":"vertical",
				 "margin":"lg",
				 "spacing":"sm",
				 "contents":[
					{
					   "type":"box",
					   "layout":"baseline",
					   "spacing":"sm",
					   "contents":[
						  {
							 "type":"text",
							 "text":"name",
							 "color":"#aaaaaa",
							 "size":"sm",
							 "flex":1
						  },
						  {
							 "type":"text",
							 "text":"%[1]s",
							 "wrap":true,
							 "color":"#666666",
							 "size":"sm",
							 "flex":5
						  }
					   ]
					},
					{
					   "type":"box",
					   "layout":"baseline",
					   "spacing":"sm",
					   "contents":[
						  {
							 "type":"text",
							 "text":"Start",
							 "color":"#aaaaaa",
							 "size":"sm",
							 "flex":1
						  },
						  {
							 "type":"text",
							 "text":"%[2]s ",
							 "wrap":true,
							 "color":"#666666",
							 "size":"sm",
							 "flex":5
						  }
					   ]
					},
					{
					   "type":"box",
					   "layout":"baseline",
					   "spacing":"sm",
					   "contents":[
						  {
							 "type":"text",
							 "text":"End",
							 "color":"#aaaaaa",
							 "size":"sm",
							 "flex":1
						  },
						  {
							 "type":"text",
							 "text":"%[3]s ",
							 "wrap":true,
							 "color":"#666666",
							 "size":"sm",
							 "flex":5
						  }
					   ]
					},
					{
					   "type":"box",
					   "layout":"baseline",
					   "spacing":"sm",
					   "contents":[
						  {
							 "type":"text",
							 "text":"Status",
							 "color":"#aaaaaa",
							 "size":"sm",
							 "flex":1
						  },
						  {
							 "type":"text",
							 "text":"%[4]s",
							 "wrap":true,
							 "color":"%[5]s",
							 "size":"sm",
							 "flex":5
						  }
					   ]
					}
				 ]
			  }
		   ]
		}
	 }`

	var _BatchName = ``
	var _BatchStatus = ``
	var _BatchStart = ``
	var _BatchEnd = ``
	var _colorStatus = ``

	for index, element := range DataForBatchGLDetailFlexMsgs {
		//===> LOOP DATA FROM API CORE
		_BatchName = DataForBatchGLDetailFlexMsgs[index].BatchName
		_BatchStatus = DataForBatchGLDetailFlexMsgs[index].BatchStatus
		_BatchStart = DataForBatchGLDetailFlexMsgs[index].BatchStart
		_BatchEnd = DataForBatchGLDetailFlexMsgs[index].BatchEnd
		_ = element
		//var _colorLog = "#1c1fd9"
		_colorStatus = "#1c1fd9"
		if _BatchStatus == "START" {
			_colorStatus = "#d3e016" //STA
		} else if _BatchStatus == "FINISH" {
			_colorStatus = "#8bc34a" //FIN
		} else if _BatchStatus == "ERROR" {
			_colorStatus = "#db2121" //ERR
		} else {
			_colorStatus = "#1c1fd9"
		}
	}

	var jsonFlexMsg = ""
	jsonFlexMsg = fmt.Sprintf(_jsonBox, _BatchName, _BatchStart, _BatchEnd, _BatchStatus, _colorStatus)
	log.Println("generateMsgBatchGLDetailByBatchName() Step7 => Return")
	return jsonFlexMsg, true
}

func getProfile(userId string) (string, string, string) {
	url := "https://api.line.me/v2/bot/profile/" + userId
	log.Println("getProfile -1")
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+os.Getenv("CHANNEL_TOKEN"))
	log.Println("getProfile -2")
	res, _ := http.DefaultClient.Do(req)
	log.Println("getProfile -3")
	defer res.Body.Close()
	log.Println("getProfile -4")
	body, _ := ioutil.ReadAll(res.Body)
	log.Println("getProfile -5")
	var profile model.Profile
	if err := json.Unmarshal(body, &profile); err != nil {
		log.Print("%% err \n")
	}
	log.Println("getProfile -6")
	log.Print(profile.DisplayName)
	return profile.DisplayName, profile.PictureURL, profile.UserID
}
