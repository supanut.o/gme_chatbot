module gme-chatbot-api

go 1.12

require (
	github.com/emersion/go-imap v1.0.4
	github.com/emersion/go-message v0.12.0
	github.com/go-delve/delve v1.4.1 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.3
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/line/line-bot-sdk-go v6.4.0+incompatible
	github.com/spf13/viper v1.6.1
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
