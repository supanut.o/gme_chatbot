package main

//UsersDetailReq struct .
type UsersDetailReq struct {
	UserID       string `json:"UserID"`
	GroupID      string `json:"GroupID"`
	RoomID       string `json:"RoomID"`
	Name         string `json:"Name"`
	Department   string `json:"Department"`
	Position     string `json:"Position"`
	Email        string `json:"Email"`
	AuthenStatus int    `json:"AuthenStatus"`
	EventMsg     string `json:"EventMsg"`
	LogEventMsg  string `json:"LogEventMsg"`
}

//UsersDetailRes struct .
type UsersDetailRes struct {
	UserID           string `json:"UserID"`
	AuthenStatus     int    `json:"AuthenStatus"`
	AuthenStatusDesc string `json:"AuthenStatusDesc"`
}

///=====>>> เอาไว้ส่ง ฌำื โสำป ?หเ
type MessageLine struct {
	UserID   string `json:"UserID"`
	EventMsg string `json:"EventMsg"`
}

///=====>>> เอาไว้ Get From DB
type DataForBatchLogFlexMsg struct {
	BatchName       string
	BatchStatus     string
	BatchTextDetail string
}

///=====>>> เอาไว้ Get From DB
type DataForBatchGLDetailFlexMsg struct {
	BatchName   string
	BatchStart  string
	BatchEnd    string
	BatchStatus string
}
