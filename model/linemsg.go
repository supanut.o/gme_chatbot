package model

///=====>>> เอาไว้ Get From DB
type MessageLine struct {
	UserID   string
	EventMsg string
}

///=====>>> เอาไว้ Get From DB
type DataForBatchLogFlexMsg struct {
	BatchName       string
	BatchStatus     string
	BatchTextDetail string
}

///=====>>> เอาไว้ Get From DB
type DataForBatchGLDetailFlexMsg struct {
	BatchName   string
	BatchStart  string
	BatchEnd    string
	BatchStatus string
}

//===> profile
type Profile struct {
	UserID        string `json:"userId"`
	DisplayName   string `json:"displayName"`
	PictureURL    string `json:"pictureURL"`
	StatusMessage string `json:"statusMessage"`
}

//===> Text
type ReplyMessage struct {
	ReplyToken string `json:"replyToken"`
	Messages   []Text `json:"messages"`
}

//===> Text
type Text struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

//===> Text
type ReplyMessageSticker struct {
	ReplyToken string    `json:"replyToken"`
	Messages   []Sticker `json:"messages"`
}

//===> Sticker
type Sticker struct {
	Type      string `json:"type"`
	PackageID string `json:"packageId"`
	StickerID string `json:"stickerId"`
}

//===> Text
type LineMessage struct {
	Destination string `json:"destination"`
	Events      []struct {
		ReplyToken string `json:"replyToken"`
		Type       string `json:"type"`
		Timestamp  int64  `json:"timestamp"`
		Source     struct {
			Type   string `json:"type"`
			UserID string `json:"userId"`
		} `json:"source"`
		Message struct {
			ID   string `json:"id"`
			Type string `json:"type"`
			Text string `json:"text"`
		} `json:"message"`
	} `json:"events"`
}
