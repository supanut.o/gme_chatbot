package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/gorilla/mux"

	"github.com/labstack/echo"
	"github.com/line/line-bot-sdk-go/linebot"

	"gme-chatbot-api/model"
	repository "gme-chatbot-api/repository"

	"github.com/spf13/viper"
)

var (
	server   = "127.0.0.1"
	port     = "3306"
	user     = "root"
	password = "P@ssw0rd"
	database = "gme_chatbot"
)

/* var (
	server   = "remotemysql.com"
	port     = "3306"
	user     = "WTGJ0SNurT"
	password = "7ZhwgEGLHg"
	database = "WTGJ0SNurT"
) */

func dbConn() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := user
	dbPass := password
	dbName := database
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+server+":"+port+")/"+dbName)
	//db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	if err != nil {
		log.Println("connect fail")
		log.Fatal("Open connection failed:", err.Error())
		panic(err.Error())
	} else {
		log.Println("connect success")
	}
	return db
}

func indexHandler(rw http.ResponseWriter, req *http.Request) {
	os.Setenv("CHANNEL_TOKEN", "9ea47ef9c6bceeddd4021c7bf4b10134")
	CHANNEL_TOKEN := os.Getenv("CHANNEL_TOKEN")
	fmt.Fprintln(rw, CHANNEL_TOKEN)
	fmt.Fprintln(rw, "Hello, Herokuxxxxx!")
}

func main_backup() {
	// Heroku supplies your port via environment variable
	port := os.Getenv("PORT")
	//os.Getenv("CHANNEL_TOKEN")

	r := mux.NewRouter()
	r.HandleFunc("/", indexHandler)
	log.Fatal(http.ListenAndServe(":"+port, r))
}

var bot *linebot.Client

func main() {
	e := echo.New()
	v, err := GetConfig("config", "./")
	//portconfig := v.GetString("server.port")
	port := os.Getenv("PORT")
	keySecret := v.GetString("linekey.secret")
	keyToken := v.GetString("linekey.token")
	//urlAPI := v.GetString("api.url")
	os.Setenv("CHANNEL_SECRET", keySecret)
	os.Setenv("CHANNEL_TOKEN", keyToken)
	os.Setenv("PORT", port)
	// fmt.Println("CHANNEL_SECRET:", os.Getenv("CHANNEL_SECRET"))
	// fmt.Println("CHANNEL_TOKEN:", os.Getenv("CHANNEL_TOKEN"))
	// fmt.Println("PORT:", os.Getenv("PORT"))

	bot, err := linebot.New(
		os.Getenv("CHANNEL_SECRET"),
		os.Getenv("CHANNEL_TOKEN"),
	)
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintln(w, "Hello, gme-chatbot-api!")
	})

	///=====> test
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "ok")
	})
	e.POST("/callbackV2_XXXX", func(c echo.Context) error {

		Line := new(model.LineMessage)
		if err := c.Bind(Line); err != nil {
			log.Println("err")
			return c.String(http.StatusOK, "error")
		}

		fullname, imageurl, userid := getProfile(Line.Events[0].Source.UserID)
		_ = imageurl
		_ = userid

		text := model.Text{
			Type: "text",
			Text: "ข้อความเข้ามา : " + Line.Events[0].Message.Text + " ยินดีต้อนรับ : " + fullname,
		}

		message := model.ReplyMessage{
			ReplyToken: Line.Events[0].ReplyToken,
			Messages: []model.Text{
				text,
			},
		}

		replyMessageLine(message)

		log.Println("%% message success")
		return c.String(http.StatusOK, "ok")

	})

	//ลองเทสใน group: event.Source.GroupID Variable: C3fced4f1cd74adf8653274938af7fd52

	http.HandleFunc("/callbackGroup", func(w http.ResponseWriter, req *http.Request) {
		eventsV3, errV3 := bot.ParseRequest(req)
		//fmt.Fprintln(w, "CHANNEL_SECRET: "+os.Getenv("CHANNEL_SECRET"))
		//fmt.Fprintln(w, "CHANNEL_TOKEN: "+os.Getenv("CHANNEL_TOKEN"))
		//fmt.Fprintln(w, "PORT: "+os.Getenv("PORT"))

		if errV3 != nil {
			if errV3 == linebot.ErrInvalidSignature {
				w.WriteHeader(400)
			} else {
				w.WriteHeader(500)
			}
			return
		}

		for _, eventV3 := range eventsV3 {
			if eventV3.Type == linebot.EventTypeMessage {
				eventMsgV3 := ""

				switch eventMessage := eventV3.Message.(type) {

				case *linebot.TextMessage:
					eventMsgV3 = eventMessage.Text
					fmt.Println(eventMsgV3)
				}

				log.Println("event.ReplyToken Variable:", eventV3.ReplyToken)
				log.Println("event.Source.GroupID Variable:", eventV3.Source.GroupID)
				//checkAutorize = 99
				// check rutorize
				// -1 = Connection error
				// 0 = User not registered
				// 1 = Waiting to process
				// 2 = Active
				// 3 = Lock usage
				// 4 = Waiting to unlock
				// 99 = Bot Sleep
				switch message := eventV3.Message.(type) {
				case *linebot.TextMessage:

					fmt.Println(strings.ToUpper(strings.TrimSpace(message.Text)))

					if message.Text == "เมนูบอท" {
						fmt.Println("เมนูบอท")
						var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","spacing":"xs","contents":[{"type":"button","style":"secondary","action":{"type":"message","label":"ISSUE","text":"ISSUE"}},{"type":"button","style":"secondary","action":{"type":"message","label":"KM","text":"KM"}},{"type":"button","style":"secondary","action":{"type":"message","label":"Batch Today","text":"BatchToday"}},{"type":"button","style":"secondary","action":{"type":"message","label":"Batch Previous","text":"Batch Previous"}}]}}`
						replyMessage(flexData, "FlexWithJSON", message.Text, eventV3.ReplyToken)
					} else {
						fmt.Println("ข้อความไม่ตรงเงื่อนไข")
						var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","paddingAll":"10px","contents":[{"type":"text","text":"Detail Batch","weight":"bold","size":"xl"},{"type":"box","layout":"vertical","margin":"lg","spacing":"sm","contents":[{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"name","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"batch_gl_01 test file test file.xls","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Start","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:10 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"End","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:25 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Status","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"Running","wrap":true,"color":"#8bc34a","size":"sm","flex":5}]}]}]}}`
						_ = flexData
						replyMessage("Test ja Group!", "FlexWithText", message.Text, "C3fced4f1cd74adf8653274938af7fd52")
						//replyMessage(flexData, "FlexWithJSON", message.Text, eventV2.Source.GroupID)
					}
				} //end switch

			}
		}

	})

	http.HandleFunc("/callbackV2", func(w http.ResponseWriter, req *http.Request) {

		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", req.Header.Get("Origin"))
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		//log.Println(string("=== GetDetailBatchGL_ByBatchNamePost() ==="))
		log.Println(req.Method)
		strUrl := req.URL
		log.Println(strUrl)
		//url := r.URL.String()
		log.Println("GET params were:", req.URL.Query())

		eventsV2, errV2 := bot.ParseRequest(req)
		//fmt.Fprintln(w, "CHANNEL_SECRET: "+os.Getenv("CHANNEL_SECRET"))
		//fmt.Fprintln(w, "CHANNEL_TOKEN: "+os.Getenv("CHANNEL_TOKEN"))
		//fmt.Fprintln(w, "PORT: "+os.Getenv("PORT"))

		if errV2 != nil {
			if errV2 == linebot.ErrInvalidSignature {
				w.WriteHeader(400)
			} else {
				w.WriteHeader(500)
			}
			return
		}

		for _, eventV2 := range eventsV2 {

			log.Println("event.Type Variable:", eventV2.Type)
			log.Println("event.Source.Type Variable:", eventV2.Source.Type)
			log.Println("event.Source.UserID Variable:", eventV2.Source.UserID)
			log.Println("event.ReplyToken Variable:", eventV2.ReplyToken)
			log.Println("event.Source.GroupID Variable:", eventV2.Source.GroupID)

			if eventV2.Type == linebot.EventTypeMessage {
				eventMsgV2 := ""
				_StickerID := ""
				_ = _StickerID
				_PackageID := ""
				_ = _PackageID
				switch eventMessage := eventV2.Message.(type) {

				case *linebot.TextMessage:
					eventMsgV2 = eventMessage.Text
					fmt.Println(eventMsgV2)
				case *linebot.StickerMessage:
					_StickerID = eventMessage.StickerID
					_PackageID = eventMessage.PackageID
					log.Println("eventMessage.StickerID:", eventMessage.StickerID)
					log.Println("eventMessage.PackageID:", eventMessage.PackageID)
				}

				//checkAutorize = 99
				// check rutorize
				// -1 = Connection error
				// 0 = User not registered
				// 1 = Waiting to process
				// 2 = Active
				// 3 = Lock usage
				// 4 = Waiting to unlock
				// 99 = Bot Sleep
				switch message := eventV2.Message.(type) {

				case *linebot.TextMessage:
					log.Println("IN CASE: linebot.TextMessage")
					log.Println(strings.ToUpper(strings.TrimSpace(message.Text)))

					confAutoSysJob := v.GetString("gme.autosysjob")
					var autoSysJob = ""
					if confAutoSysJob == "" {
						autoSysJob = `GL|NES|TRS_DWH_TRSMARGIN`
					}

					var re = regexp.MustCompile(autoSysJob)
					subStrKeywordFix := "STATUS"
					message.Text = strings.ToUpper(strings.TrimSpace(message.Text))

					if message.Text == "เมนูบอท" {
						log.Println("เมนูบอท")
						var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","spacing":"xs","contents":[{"type":"button","style":"secondary","action":{"type":"message","label":"ISSUE","text":"ISSUE"}},{"type":"button","style":"secondary","action":{"type":"message","label":"KM","text":"KM"}},{"type":"button","style":"secondary","action":{"type":"message","label":"Batch Today","text":"BatchToday"}},{"type":"button","style":"secondary","action":{"type":"message","label":"Batch Previous","text":"Batch Previous"}}]}}`
						replyMessage(flexData, "FlexWithJSON", message.Text, eventV2.ReplyToken)
					} else if message.Text == "หวยออกไร" {
						log.Println("หวยออกไร")
						var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","paddingAll":"10px","contents":[{"type":"text","text":"Detail Batch","weight":"bold","size":"xl"},{"type":"box","layout":"vertical","margin":"lg","spacing":"sm","contents":[{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"name","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"batch_gl_01 test file test file.xls","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Start","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:10 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"End","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:25 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Status","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"Running","wrap":true,"color":"#8bc34a","size":"sm","flex":5}]}]}]}}`
						_ = flexData
						replyMessage("บน 516967 ล่าง 64", "FlexWithText", message.Text, eventV2.ReplyToken)
						//replyMessage(flexData, "FlexWithJSON", message.Text, eventV2.Source.GroupID)
					} else if message.Text == "TEST" {
						log.Println("Test")
						var jsonstr = `{"message":"` + message.Text + `"}`
						dataStr := repository.RequestMailImapBLL(jsonstr)
						_ = dataStr //===> ได้ข้อมูลการ Save ออกมา

						replyMessage(dataStr, "FlexWithText", message.Text, eventV2.ReplyToken)
					} else if message.Text == "-H" {
						log.Println("-H")
						var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","spacing":"xs","contents":[{"type":"button","style":"secondary","action":{"type":"message","label":"TRS_DWH_TRSMARGIN","text":"STATUS TRS_DWH_TRSMARGIN"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"GL","text":"STATUS GL"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"NES","text":"STATUS NES"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"เลข","text":"หวยออกไร"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"เลข","text":"หวยออกไร"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"uri","label":"Covid-19","uri":"https://covid19.th-stat.com/th/share/dashboard","altUri":{"desktop":"https://covid19.th-stat.com/th/share/dashboard"}},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"Job AutoSys 1","text":"Job AutoSys 1"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"Job AutoSys 2","text":"Job AutoSys 2"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"Job AutoSys 3","text":"Job AutoSys 3"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"Job AutoSys 4","text":"Job AutoSys 4"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"Job AutoSys 5","text":"Job AutoSys 5"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"Job AutoSys 6","text":"Job AutoSys 6"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"Job AutoSys 7","text":"Job AutoSys 7"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"Job AutoSys 8","text":"Job AutoSys 8"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"Job AutoSys 9","text":"Job AutoSys 9"},"height":"sm","color":"#99FF99"},{"type":"button","style":"secondary","action":{"type":"message","label":"Job AutoSys 10","text":"ทดสอบ 10"},"height":"sm","color":"#99FF99"}]}}`
						replyMessage(flexData, "FlexWithJSON", message.Text, eventV2.ReplyToken)
					} else if message.Text == "-H2" {
						log.Println("-H2")
						var flexData = `{"type":"carousel","contents":[{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"text","text":"ABYSS_CHASER.","wrap":true}]},"footer":{"type":"box","layout":"horizontal","contents":[{"type":"button","style":"primary","action":{"type":"uri","label":"Go","uri":"https://example.com"}}]}},{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"text","text":"ARCH_MAGE.","wrap":true}]},"footer":{"type":"box","layout":"horizontal","contents":[{"type":"button","style":"primary","action":{"type":"uri","label":"Go","uri":"https://example.com"}}]}},{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"text","text":"BIOLO .","wrap":true}]},"footer":{"type":"box","layout":"horizontal","contents":[{"type":"button","style":"primary","action":{"type":"uri","label":"Go","uri":"https://example.com"}}]}},{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"text","text":"CARDINAL.","wrap":true}]},"footer":{"type":"box","layout":"horizontal","contents":[{"type":"button","style":"primary","action":{"type":"uri","label":"Go","uri":"https://example.com"}}]}},{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"text","text":"DRAGON_KNIGHT.","wrap":true}]},"footer":{"type":"box","layout":"horizontal","contents":[{"type":"button","style":"primary","action":{"type":"uri","label":"Go","uri":"https://example.com"}}]}},{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"text","text":"ELEMETAL_MASTER.","wrap":true}]},"footer":{"type":"box","layout":"horizontal","contents":[{"type":"button","style":"primary","action":{"type":"uri","label":"Go","uri":"https://example.com"}}]}},{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"text","text":"IMPERIAL_GUARD.","wrap":true}]},"footer":{"type":"box","layout":"horizontal","contents":[{"type":"button","style":"primary","action":{"type":"uri","label":"Go","uri":"https://example.com"}}]}},{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"text","text":"INQUISITOR.","wrap":true}]},"footer":{"type":"box","layout":"horizontal","contents":[{"type":"button","style":"primary","action":{"type":"uri","label":"Go","uri":"https://example.com"}}]}},{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"text","text":"WINDHAWK.","wrap":true}]},"footer":{"type":"box","layout":"horizontal","contents":[{"type":"button","style":"primary","action":{"type":"uri","label":"Go","uri":"https://example.com"}}]}},{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"text","text":"SHADOW_CROSS.","wrap":true}]},"footer":{"type":"box","layout":"horizontal","contents":[{"type":"button","style":"primary","action":{"type":"uri","label":"Go","uri":"https://example.com"}}]}}]}`
						replyMessage(flexData, "FlexWithJSON", message.Text, eventV2.ReplyToken)
					} else if message.Text == "-CONFIG.YML" {
						log.Println("-config.yml")

						//v, _ := GetConfig("config", "./")
						confmsgserverport := v.GetString("server.port")
						confmsggmeautosysjob := v.GetString("gme.autosysjob")
						confmsggmemailhost := v.GetString("gme.mailhost")
						confmsggmemailuser := v.GetString("gme.mailuser")
						//confmsggmemailpassword := v.GetString("gme.mailpassword")
						confmsggmemailpassword := "XXXXX"
						confmsggmemailbox := v.GetString("gme.mailbox")

						var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","paddingAll":"10px","contents":[{"type":"text","text":"config.yml","weight":"bold","size":"xl"},{"type":"box","layout":"vertical","margin":"sm","spacing":"xs","contents":[{"type":"box","layout":"vertical","spacing":"sm","contents":[{"type":"text","text":"server.port","color":"#aaaaaa","size":"xs","flex":6},{"type":"text","text":"` + confmsgserverport + `","wrap":true,"color":"#666666","size":"sm","flex":6}]},{"type":"box","layout":"vertical","spacing":"sm","contents":[{"type":"text","text":"gme.autosysjob","color":"#aaaaaa","size":"sm","flex":6},{"type":"text","text":"` + confmsggmeautosysjob + `","wrap":true,"color":"#666666","size":"sm","flex":6}]},{"type":"box","layout":"vertical","spacing":"sm","contents":[{"type":"text","text":"gme.mailhost","color":"#aaaaaa","size":"sm","flex":6},{"type":"text","text":"` + confmsggmemailhost + `","wrap":true,"color":"#666666","size":"sm","flex":6}]},{"type":"box","layout":"vertical","spacing":"sm","contents":[{"type":"text","text":"gme.mailuser","color":"#aaaaaa","size":"sm","flex":6},{"type":"text","text":"` + confmsggmemailuser + `","wrap":true,"color":"#666666","size":"sm","flex":6}]},{"type":"box","layout":"vertical","spacing":"sm","contents":[{"type":"text","text":"gme.mailpassword","color":"#aaaaaa","size":"sm","flex":6},{"type":"text","text":"` + confmsggmemailpassword + `","wrap":true,"color":"#666666","size":"sm","flex":6}]},{"type":"box","layout":"vertical","spacing":"sm","contents":[{"type":"text","text":"gme.mailbox","color":"#aaaaaa","size":"sm","flex":6},{"type":"text","text":"` + confmsggmemailbox + `","wrap":true,"color":"#666666","size":"sm","flex":6}]}]}]}}`
						replyMessage(flexData, "FlexWithJSON", message.Text, eventV2.ReplyToken)
					} else {
						log.Println("Message Not In Coding Case.")
						//replyMessage(flexData, "FlexWithJSON", message.Text, eventV2.Source.GroupID)
						if strings.Contains(message.Text, subStrKeywordFix) && re.MatchString(message.Text) {
							//===> ถ้าเจอคำว่า Test และตาม Regex
							log.Println("Message is contain subStrKeywordFix=STATUS and contain AutosysJob Type.")
							log.Println("Found subStr in str.")
							log.Println(message.Text)
							var jsonstr = `{"message":"` + message.Text + `"}`
							log.Println("Before Get Mail and Insert Data to Database.")
							dataStr := repository.RequestMailImapBLL(jsonstr)
							_ = dataStr //===> ได้ข้อมูลการ Save ออกมา
							myText := strings.Replace(message.Text, subStrKeywordFix, "", 1)
							myText = strings.TrimSpace(myText)
							log.Println("Replae subStrKeywordFix=STATUS with AutoSysJobName-" + myText + "and get Message from Database.")
							dataStrRet := repository.CheckCriteriaMsgAutoSysTypeWithMsgBLL(myText)
							_ = dataStrRet //===> ได้ข้อมูลการ Save ออกมา
							replyMessage(dataStrRet, "FlexWithText", message.Text, eventV2.ReplyToken)
						} else {
							log.Println("Message is not contain subStrKeywordFix=STATUS and contain AutosysJob Type.")
							//===> ถ้าไม่เจอ *** ถ้าใช้งานจริงต้องไม่มีการทำอะไร ***
							log.Println("Not Found subStr in str.")
							var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","paddingAll":"10px","contents":[{"type":"text","text":"Detail Batch","weight":"bold","size":"xl"},{"type":"box","layout":"vertical","margin":"lg","spacing":"sm","contents":[{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"name","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"batch_gl_01 test file test file.xls","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Start","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:10 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"End","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:25 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Status","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"Running","wrap":true,"color":"#8bc34a","size":"sm","flex":5}]}]}]}}`
							_ = flexData
							//replyMessage(message.Text, "FlexWithText", message.Text, eventV2.ReplyToken)

							fullname, imageurl, userid := getProfile(eventV2.Source.UserID)
							log.Println("fullname: ", fullname)
							log.Println("imageurl: ", imageurl)
							log.Println("userid: ", userid)

							var genmsg = ""
							if fullname == "" {
								genmsg = "คุณเป็นใคร " + message.Text + " หมายความว่ายังไง?"
							} else {
								genmsg = "สวัสดีคุณ - " + fullname + " - " + message.Text + " หมายความว่ายังไง?"
							}

							text := model.Text{
								Type: "text",
								Text: genmsg,
							}
							log.Println("eventV2.ReplyToken: ", eventV2.ReplyToken)
							message := model.ReplyMessage{
								ReplyToken: eventV2.ReplyToken,
								Messages: []model.Text{
									text,
								},
							}
							replyMessageLine(message)

						}
					}
				case *linebot.StickerMessage:
					//===> *** ถ้าใช้งานจริงต้องไม่มีการทำอะไร ***
					log.Println("IN CASE: linebot.StickerMessage")
					log.Println("message.StickerID:", message.StickerID)
					log.Println("message.PackageID:", message.PackageID)
					var flexData = `{"type":"sticker","id": "","packageId":"` + message.PackageID + `","stickerId":"` + message.StickerID + `","stickerResourceType": "STATIC"}`
					_ = flexData
					log.Println("flexData:", flexData)
					//replyMessage("ไม่เข้าใจรูปภาพอ่ะ", "FlexWithText", "StickerMessage", eventV2.ReplyToken)
					//replyMessage(flexData, "Sticker", "StickerMessage", eventV2.ReplyToken)

					_sticker := model.Sticker{
						Type:      "sticker",
						StickerID: message.StickerID,
						PackageID: message.PackageID,
					}
					log.Println("eventV2.ReplyToken: ", eventV2.ReplyToken)
					messageSticker := model.ReplyMessageSticker{
						ReplyToken: eventV2.ReplyToken,
						Messages: []model.Sticker{
							_sticker,
						},
					}
					_ = messageSticker

					errSticker, retStrSticket := replyMessageStickerLine(messageSticker)
					_ = errSticker
					_ = retStrSticket

					/*
						jsonMap := make(map[string]interface{})
						errmsgData := json.Unmarshal([]byte(retStrSticket), &jsonMap)
						_ = errmsgData
						var _message string
						for key, value := range jsonMap {
							log.Println("index : ", key, " - value : ", value)
							if key == "message" {
								_message = value.(string)
							}
						}
						if strings.Contains(_message, "invalid") {
							fullname, imageurl, userid := getProfile(eventV2.Source.UserID)
							_ = userid
							var flexData = `{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"image","url":"` + imageurl + `","size":"md"}]}}`
							_ = flexData
							replyMessage(flexData, "FlexWithJSON", fullname, eventV2.ReplyToken)
						}
					*/

					/* Send Flex Message เป็น Image Profile ของตัวเอง
					fullname, imageurl, userid := getProfile(eventV2.Source.UserID)
					_ = userid
					var flexDataImage = `{"type":"bubble","body":{"type":"box","layout":"horizontal","contents":[{"type":"image","url":"` + imageurl + `","size":"md"}]}}`
					_ = flexDataImage
					replyMessage(flexDataImage, "FlexWithJSON", fullname, eventV2.ReplyToken)
					*/

				} //end switch

			}
		}

	})

	// Setup HTTP Server for receiving requests from LINE platform
	http.HandleFunc("/callback", func(w http.ResponseWriter, req *http.Request) {
		events, err := bot.ParseRequest(req)
		//fmt.Fprintln(w, "CHANNEL_SECRET: "+os.Getenv("CHANNEL_SECRET"))
		//fmt.Fprintln(w, "CHANNEL_TOKEN: "+os.Getenv("CHANNEL_TOKEN"))
		//fmt.Fprintln(w, "PORT: "+os.Getenv("PORT"))

		if err != nil {
			if err == linebot.ErrInvalidSignature {
				w.WriteHeader(400)
			} else {
				w.WriteHeader(500)
			}
			return
		}

		//fmt.Println(linebot.EventTypeMessage)
		for _, event := range events {
			if event.Type == linebot.EventTypeMessage {
				eventMsg := ""

				switch eventMessage := event.Message.(type) {

				case *linebot.TextMessage:
					eventMsg = eventMessage.Text
					fmt.Println(eventMsg)
				}

				// check authorize
				checkAutorize, AuthenDetail := CheckAuthorize(event.Source.UserID, event.Source.GroupID, event.Source.RoomID, eventMsg, "event.Message")
				log.Println("checkAutorize Variable:", checkAutorize)
				log.Println("AuthenDetail Variable:", AuthenDetail)
				log.Println("event.ReplyToken Variable:", event.ReplyToken)
				log.Println("event.Source.GroupID Variable:", event.Source.GroupID)
				//checkAutorize = 99
				// check rutorize
				// -1 = Connection error
				// 0 = User not registered
				// 1 = Waiting to process
				// 2 = Active
				// 3 = Lock usage
				// 4 = Waiting to unlock
				// 99 = Bot Sleep

				if checkAutorize != 99 {
					if checkAutorize == -1 {
						replyMessage(AuthenDetail, "", "", event.ReplyToken)
					} else if checkAutorize == 1 {
						fmt.Println(checkAutorize)
						replyMessage(AuthenDetail, "", "", event.ReplyToken)
					} else if checkAutorize == 2 {

						switch message := event.Message.(type) {
						case *linebot.TextMessage:

							fmt.Println(strings.ToUpper(strings.TrimSpace(message.Text)))

							if message.Text == "เมนูบอท" {
								fmt.Println("เมนูบอท")
								var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","spacing":"xs","contents":[{"type":"button","style":"secondary","action":{"type":"message","label":"ISSUE","text":"ISSUE"}},{"type":"button","style":"secondary","action":{"type":"message","label":"KM","text":"KM"}},{"type":"button","style":"secondary","action":{"type":"message","label":"Batch Today","text":"BatchToday"}},{"type":"button","style":"secondary","action":{"type":"message","label":"Batch Previous","text":"Batch Previous"}}]}}`
								replyMessage(flexData, "FlexWithJSON", message.Text, event.ReplyToken)
							} else if strings.ToUpper(strings.TrimSpace(message.Text)) == "MENUBOT" {
								fmt.Println("MENUBOT")
								var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","spacing":"xs","contents":[{"type":"button","style":"secondary","action":{"type":"message","label":"ISSUE","text":"ISSUE"}},{"type":"button","style":"secondary","action":{"type":"message","label":"KM","text":"KM"}},{"type":"button","style":"secondary","action":{"type":"message","label":"Batch Today","text":"BatchToday"}},{"type":"button","style":"secondary","action":{"type":"message","label":"Batch Previous","text":"Batch Previous"}}]}}`
								replyMessage(flexData, "FlexWithJSON", message.Text, event.ReplyToken)
							} else if strings.ToUpper(strings.TrimSpace(message.Text)) == "BATCHTODAY" {
								fmt.Println("BATCHTODAY")
								var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","spacing":"xs","contents":[{"type":"button","style":"secondary","action":{"type":"message","label":"GL","text":"Batch_GL"}},{"type":"button","style":"secondary","action":{"type":"message","label":"PLUD","text":"Batch_PLUD"}},{"type":"button","style":"secondary","action":{"type":"message","label":"RCFX","text":"Batch_RCFX"}},{"type":"button","style":"secondary","action":{"type":"message","label":"BOT","text":"Batch_BOT"}},{"type":"button","style":"secondary","action":{"type":"message","label":"CBS","text":"Batch_CBS"}}]}}`
								replyMessage(flexData, "FlexWithJSON", message.Text, event.ReplyToken)

							} else if strings.Contains(strings.ToUpper(message.Text), "DETAILBATCHGL0") {
								//===> GET FLEX MESSAGE WITH DATA FROM SPROC
								var flexData = ``
								flexData, result := generateMsgBatchGLDetailByBatchName(event.Source.UserID, message.Text)
								_ = result
								//var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","paddingAll":"10px","contents":[{"type":"text","text":"Detail Batch","weight":"bold","size":"xl"},{"type":"box","layout":"vertical","margin":"lg","spacing":"sm","contents":[{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"name","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"batch_gl_01 test file test file.xls","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Start","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:10 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"End","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:25 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Status","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"Running","wrap":true,"color":"#8bc34a","size":"sm","flex":5}]}]}]}}`
								replyMessage(flexData, "FlexWithJSON", message.Text, event.ReplyToken)
							} else if strings.ToUpper(message.Text) == "BATCH_GL_DYN" {
								//===> GET FLEX MESSAGE WITH DATA FROM SPROC
								//var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","paddingAll":"10px","contents":[{"type":"text","text":"Detail Batch","weight":"bold","size":"xl"},{"type":"box","layout":"vertical","margin":"lg","spacing":"sm","contents":[{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"name","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"batch_gl_01 test file test file.xls","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Start","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:10 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"End","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:25 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Status","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"Running","wrap":true,"color":"#8bc34a","size":"sm","flex":5}]}]}]}}`
								var flexData = ``
								v, _ := GetConfig("config", "./")
								urlDetail := v.GetString("api.detail")
								flexData, result := generateMsgBatchLogStatus(urlDetail, event.Source.UserID, message.Text)
								//flexData, result := generateMsgBatchLogStatusTest(urlDetail, event.Source.UserID, message.Text)
								_ = result
								replyMessage(flexData, "FlexWithJSON", message.Text, event.ReplyToken)
							} else if strings.Contains(strings.ToUpper(message.Text), "BATCH_GL") {
								//===> GET FLEX MESSAGE WITH DATA FROM SPROC
								//var flexData = `{"type":"bubble","body":{"type":"box","layout":"vertical","paddingAll":"10px","contents":[{"type":"text","text":"Detail Batch","weight":"bold","size":"xl"},{"type":"box","layout":"vertical","margin":"lg","spacing":"sm","contents":[{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"name","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"batch_gl_01 test file test file.xls","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Start","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:10 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"End","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"25-11-2019 21:25 ","wrap":true,"color":"#666666","size":"sm","flex":5}]},{"type":"box","layout":"baseline","spacing":"sm","contents":[{"type":"text","text":"Status","color":"#aaaaaa","size":"sm","flex":1},{"type":"text","text":"Running","wrap":true,"color":"#8bc34a","size":"sm","flex":5}]}]}]}}`
								var flexData = ``
								v, _ := GetConfig("config", "./")
								urlDetail := v.GetString("api.detail")
								flexData, result := generateMsgBatchLogStatus(urlDetail, event.Source.UserID, message.Text)
								//flexData, result := generateMsgBatchLogStatusTest(urlDetail, event.Source.UserID, message.Text)
								_ = result
								replyMessage(flexData, "FlexWithJSON", message.Text, event.ReplyToken)
							} else {
								fmt.Println("ข้อความไม่ตรงเงื่อนไข")
							}
						} //end switch

					} else if checkAutorize == 3 {
						if event.Source.GroupID == "" {
							msgUnactive(event.Source.UserID, AuthenDetail, event.ReplyToken)
						} else {
							//replyMessage(AuthenDetail, "", "", event.ReplyToken)
							msgUnactiveRedirectToPrivateChatBot(event.Source.UserID, AuthenDetail, event.ReplyToken)
						}
					} else if checkAutorize == 4 {
						replyMessage(AuthenDetail, "", "", event.ReplyToken)
					} else if checkAutorize == 5 {
						replyMessage(AuthenDetail, "", "", event.ReplyToken)
					} else if checkAutorize == 6 {
						replyMessage(AuthenDetail, "", "", event.ReplyToken)
					} else if checkAutorize == 0 {
						if event.Source.GroupID == "" {
							// not register
							msgRegister(event.Source.UserID, AuthenDetail, event.ReplyToken)
						} else {
							//replyMessage(AuthenDetail, "", "", event.ReplyToken)
							msgRegisterRedirectToPrivateChatBot(event.Source.UserID, AuthenDetail, event.ReplyToken)
						}
					} else {
						if event.Source.GroupID == "" {
							// not register
							msgRegister(event.Source.UserID, AuthenDetail, event.ReplyToken)
						} else {
							replyMessage(AuthenDetail, "", "", event.ReplyToken)
						}
					}
				}
			}
		}
	})
	// This is just sample code.
	// For actual use, you must support HTTPS by using `ListenAndServeTLS`, a reverse proxy or something else.

	log.Println("Starting")
	//if err := http.ListenAndServe(":"+os.Getenv("PORT"), nil); err != nil {
	if err := http.ListenAndServe(getPort(), nil); err != nil {
		log.Fatal(err)
		e.Logger.Fatal(e.Start(":6666"))
	}
}

func getPort() string {
	v, err := GetConfig("config", "./")
	_ = err
	var portconfig = v.GetString("server.port")
	var port = os.Getenv("PORT")
	if port == "" {
		log.Println("No Port In Heroku os.Getenv(\"PORT\")")
		//port = "8080"
		if portconfig == "" {
			log.Println("Do Not Exists Port In config.yml File and Fix to port 6666")
			port = "6666"
		} else {
			log.Println("Exists Port In config.yml File")
			port = portconfig
		}
	}
	log.Println("Result Start With Port :" + port)
	return ":" + port
}

// GetConfig Get Config
func GetConfig(fileName string, filePath string) (*viper.Viper, error) {
	v := viper.New()
	v.SetConfigName(fileName)
	v.AddConfigPath(filePath)
	v.AutomaticEnv()
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := v.ReadInConfig()

	if err != nil {
		return nil, err
	}
	return v, nil
}

/* #region GET_MAIL_WITH_IMAP */
func RequestMailImapPost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== RequestMailImapPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		dataStr := repository.RequestMailImapBLL(jsonStr)
		_ = dataStr
		w.Write([]byte(dataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */

func replyMessageLine(Message model.ReplyMessage) error {
	value, _ := json.Marshal(Message)
	log.Println(string(value))
	url := "https://api.line.me/v2/bot/message/reply"
	channeltoken := os.Getenv("CHANNEL_TOKEN")
	log.Println("channeltoken: " + channeltoken)
	var jsonStr = []byte(value)
	//log.Println(string(jsonStr))
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+channeltoken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil
	}
	defer resp.Body.Close()

	log.Println("response Status:", resp.Status)
	log.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	log.Println("response Body:", string(body))

	return err
}

func replyMessageStickerLine(Message model.ReplyMessageSticker) (error, string) {
	value, _ := json.Marshal(Message)
	log.Println(string(value))
	url := "https://api.line.me/v2/bot/message/reply"
	channeltoken := os.Getenv("CHANNEL_TOKEN")
	log.Println("channeltoken: " + channeltoken)
	var jsonStr = []byte(value)
	//log.Println(string(jsonStr))
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+channeltoken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, ""
	}
	defer resp.Body.Close()

	log.Println("response Status:", resp.Status)
	log.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	log.Println("response Body:", string(body))

	return err, string(body)
}

///=====>>> เอาไว้บันทึกลง DB
type MailMsgForSaveDB struct {
	SeqSet       int
	MsgUid       string
	MsgDate      string
	MsgSubject   string
	MsgFrom      string
	MsgSender    string
	MsgReplyTo   string
	MsgTo        string
	MsgCc        string
	MsgBcc       string
	MsgInReplyTo string
	MsgMessageId string
	MsgBody      string
}

///=====>>> เอาไว้แสดงผลตอนดึงค่าออกมาจาก DB
type MailMsgFetchMailView struct {
	ObjectID       int
	MsgUid         string
	MsgDate        string
	MsgSubject     string
	MsgMessageId   string
	ResultDescView string
}
